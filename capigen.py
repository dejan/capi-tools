#!/usr/bin/env python3
"""
A very simple tool that extracts functions and objects from a shared library, and generates the
initial CAPI file ready to be edited further...
"""
from elftools.elf.elffile import ELFFile
from elftools.elf.sections import NoteSection, SymbolTableSection, SymbolTableIndexSection
from elftools.elf.descriptions import describe_symbol_type, describe_symbol_bind, describe_symbol_visibility
from elftools.elf.descriptions import describe_symbol_shndx
from dataclasses import dataclass
from operator import attrgetter
import sys
import yaml
from typing import List
from pathlib import Path

from capi_tools import Record
from capi_tools.version import get_version
from capi_tools.fs import check_outpath
from capi_tools.capi_functions import process_functions


def generate_records(lib_so_file: str, stats: list) -> List[Record]:
    records = []
    with open(lib_so_file, 'rb') as file:
        elffile = ELFFile(file)
        if elffile.elfclass == 32:
            # 32bit
            print("32bit ELF")
        else:
            # 64bit
            print("64bit ELF")

        symbol_tables = [(idx, s) for idx, s in enumerate(elffile.iter_sections())
                         if isinstance(s, SymbolTableSection)]

        if not symbol_tables and elffile.num_sections() == 0:
            print("Dynamic symbol information is not available for displaying symbols.")
            exit(0)

        for section_index, section in symbol_tables:
            if not isinstance(section, SymbolTableSection):
                continue

            for nsym, symbol in enumerate(section.iter_symbols()):
                include = False
                st_value = int(symbol['st_value'])
                record_type = describe_symbol_type(symbol['st_info']['type'])
                if record_type == "OBJECT":
                    stats[0] += 1
                    include = True
                if record_type == "FUNC":
                    stats[1] += 1
                    include = True
                # We only want objects and functions
                if st_value > 0 and include:
                    records.append(Record(int(nsym), st_value,
                                          record_type,
                                          describe_symbol_bind(symbol['st_info']['bind']),
                                          describe_symbol_visibility(symbol['st_other']['visibility']),
                                          symbol.name))
    return records


def gen_objects(sorted_recs) -> dict:
    ret = {}
    for rec in sorted_recs:
        if rec.rec_type == "OBJECT":
            obj_rec = {"type": "object", "doc": "TODO: write doc", "ignore": True}
            ret[rec.name] = obj_rec
    return ret


def gen_functions(sorted_recs) -> dict:
    ret = {}
    for rec in sorted_recs:
        if rec.rec_type == "FUNC":
            fun_rec = {"doc": "TODO: function docstring",
                       "parameters": [
                           {"name": "first",
                            "type": "int",
                            "doc": "TODO: write doc for first parameter"
                            }
                       ],
                       "return": {"type": "Unknown",
                                  "doc": "TODO: write doc for return type"
                                  },

                       "ignore": True}
            ret[rec.name] = fun_rec
    return ret


if len(sys.argv) <= 1:
    print("./capigen.py <name> [sopath] [outpath]")
    print("  name - name of the library. Example: lz4 (for liblz4)")
    print("  sopath - full path to the shared library. Example: /lib/x86_64-linux-gnu/libbz2.so.1.0")
    print("           Defaults to /usr/lib64/lib<name>.so")
    print("  outpath - Path where the output file is saved. Defaults to ./generated")
    print("            After successful exit <outpath>/capi/<name> will contain capi files.")
    exit(1)

lib_name = sys.argv[1]

lib_so_file = "/usr/lib64/lib" + lib_name + ".so"
if len(sys.argv) > 2:
    lib_so_file = sys.argv[2]

out_dir_name = Path("./generated").as_posix()
if len(sys.argv) > 3:
    out_dir_name = sys.argv[3]

check_outpath(lib_name, out_dir_name)

stats = [0, 0]  # number of objects, number of functions
records = generate_records(lib_so_file, stats)

sorted_records = sorted(records, key=attrgetter('name'))

process_functions(lib_name, sorted_records, Path(out_dir_name))

out_file_name = out_dir_name + "/" + lib_name + ".capi"

capi_data = {"name": lib_name, "source": lib_so_file, "doc": "TODO: module doc"}
print(f"Generating CAPI file for lib{lib_name} in {out_file_name}")

# If we have some objects put them first
if stats[0] > 0:
    capi_data["objects"] = gen_objects(sorted_records)

# After objects we put functions
if stats[1] > 0:
    capi_data["functions"] = gen_functions(sorted_records)

ver_tuple = get_version(lib_name, lib_so_file)
capi_data["version"] = ver_tuple

with open(out_file_name, "w") as out_file:
    yaml.safe_dump(capi_data, out_file)
