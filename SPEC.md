CAPI format specification
=========================

CAPI files are YAML files containing lots of multi-line docs.

Multi-line docstrings:

Example:

````
functions:
  blosc_get_version_string:
    doc: |-
      A docstring for the blosc_get_version_string() function.
      Multi-line.
    ignore: false
    parameters:
    - doc: 'TODO: write doc for first parameter'
      name: first
      type: int
    return:
      doc: 'TODO: write doc for return type'
      type: Unknown
name: blosc
objects: {}
source: /usr/lib/x86_64-linux-gnu/libblosc.so.1
version:
- 1
- 21
- 1      
````
