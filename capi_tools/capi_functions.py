from typing import List
from pathlib import Path

from capi_tools import Record


def get_capi_files(lib_name:str, out_path: Path) -> list:
    result = [capi_file.stem for capi_file in out_path.glob("*.capi")]
    return result

def process_functions(lib_name: str, records: List[Record], out_path: Path):
    capi_fun_dir = out_path / "capi" / lib_name / "functions"
    # these functions have already been documented (ie .capi files exist)
    doc_fun_list = get_capi_files(lib_name, capi_fun_dir)
    
    # Functions not yet documented, or intentionally ignored (ie. no .capi file for them)
    ign_fun_list = []
    for record in records:
        if record.rec_type == 'FUNC':
            if not record.name in doc_fun_list:
                ign_fun_list.append(record.name)

    print("DOCUMENTED:")
    for fun in doc_fun_list:
        print(fun)
    print("IGNORED:")
    for fun in ign_fun_list:
        print(fun)

    # If there are some functions that should be in the ignore list, write them to the
    # ignore.list file
    if len(ign_fun_list) > 0:
        ignore_lst_file_path = capi_fun_dir / "ignore.list"
        with ignore_lst_file_path.open("w") as ignore_lst_file:
            for ifun in ign_fun_list:
                ignore_lst_file.write(ifun)
                ignore_lst_file.write("\n")
