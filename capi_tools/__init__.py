from dataclasses import dataclass


__version__ = "0.1.2"


@dataclass
class Record:
    num: int
    st_value: int
    rec_type: str = "FUNC"
    bind: str = "GLOBAL"
    visibility: str = "DEFAULT"
    name: str = "UNKNOWN"
