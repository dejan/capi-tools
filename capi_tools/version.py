from ctypes import CDLL, c_int, byref, c_char_p


def bz2(so_file_name: str = "libbz2.so") -> tuple:
    libbz2 = CDLL(so_file_name)
    libbz2.BZ2_bzlibVersion.restype = c_char_p
    bz2_version = libbz2.BZ2_bzlibVersion()
    ver = bz2_version.decode("utf-8")
    verstr = ver.split(",")[0]
    parts = verstr.split(".")
    return int(parts[0]), int(parts[1]), int(parts[2])


def hdf5(so_file_name: str = "libhdf5.so") -> tuple:
    libhdf5 = CDLL(so_file_name)
    cm = c_int(0)
    cn = c_int(0)
    cr = c_int(0)
    libhdf5.H5get_libversion(byref(cm), byref(cn), byref(cr))
    return cm.value, cn.value, cr.value


def lz4(so_file_name: str = "liblz4.so.1") -> tuple:
    liblz4 = CDLL(so_file_name)
    vn = liblz4.LZ4_versionNumber()
    v = int(vn)
    m = int(v / 10000)
    v = v % 10000
    n = int(v / 100)
    v = v % 100
    r = v
    return m, n, r


def tar(so_file_name: str = "libtar.so.1") -> tuple:
    libtar = CDLL(so_file_name)
    # libtar exports a const char* string as its version
    # NOTE: not a function!
    ver = c_char_p.in_dll(libtar, "libtar_version")
    x = bytes(ver)
    ver_str = x.decode("utf-8")
    ver_str = ver_str.strip("\x00")
    parts = ver_str.split(".")
    return int(parts[0]), int(parts[1]), int(parts[2])


def blosc(so_file_name: str = "libblosc.so.1") -> tuple:
    libblosc = CDLL(so_file_name)
    libblosc.blosc_get_version_string.restype = c_char_p
    vn = libblosc.blosc_get_version_string()
    x = bytes(vn)
    ver_str = x.decode("utf-8")
    parts = ver_str.split(".")
    return int(parts[0]), int(parts[1]), int(parts[2])


def ncurses(so_file_name: str = "libncurses.so.6") -> tuple:
    # curses_version() returns string in form: "ncurses 6.3.20211021"
    libncurses = CDLL(so_file_name)
    libncurses.curses_version.restype = c_char_p
    vn = libncurses.curses_version()
    x = bytes(vn)
    ver_str = x.decode("utf-8")
    parts = ver_str.split(".")
    major = parts[0][8:]
    return int(major), int(parts[1]), int(parts[2])


# Below is on Ubuntu, which does not have .so.1 or similar...
# const char *pcap_lib_version(void);
def pcap(so_file_name: str = "libpcap.so") -> tuple:
    libpcap = CDLL(so_file_name)
    libpcap.pcap_lib_version.restype = c_char_p
    vn = libpcap.pcap_lib_version()
    x = bytes(vn)
    ver_str = x.decode("utf-8")
    print(ver_str)
    ver_str = ver_str.strip("libpcap version ")
    parts = ver_str.split(".")
    last_part: str = parts[2]

    patch_version = 0
    oparent_loc = last_part.index(" (")
    if oparent_loc == 0:
        oparent_loc = last_part.index("-")
    if oparent_loc > 0:
        patch_version = int(last_part[0:oparent_loc])

    return int(parts[0]), int(parts[1]), patch_version


def get_version(lib_name: str, so_file_name: str):
    if lib_name == "bz2":
        return bz2(so_file_name)
    if lib_name == "hdf5":
        return hdf5(so_file_name)
    if lib_name == "lz4":
        return lz4(so_file_name)
    if lib_name == "tar":
        return tar(so_file_name)
    if lib_name == "blosc":
        return blosc(so_file_name)
    if lib_name == "ncurses":
        return ncurses(so_file_name)
    if lib_name == "pcap":
        return pcap(so_file_name)

    return 0, 0, 0
