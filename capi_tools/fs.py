# Various file-system related functions

from pathlib import Path


def check_outpath(lib_name: str, output_dir_name: str):
    output_path = Path(output_dir_name)
    print(output_path)
    functions_path = output_path / "capi" / lib_name / "functions"
    if not functions_path.exists():
        functions_path.mkdir(parents=True)
