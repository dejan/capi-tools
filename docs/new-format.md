CAPI New Format
===============

For an example, directory layout for libbz2 should look like:

```
bz2
|-functions
| |-BZ2_blockSort.capi
| |-BZ2_bsInitWrite.capi
| |-BZ2_bzBuffToBuffCompress.capi
| |-BZ2_bzCompressInit.capi
| |-BZ2_indexIntoF.capi
| `-ignore.list
|-objects
| |-BZ2_crc32Table.capi
| `-BZ2_rNums.capi
`-types
  |-bz_stream.capi
  |-TypeA.capi
  `-TypeB.capi
```

BZ2_bzCompressInit.capi file looks like:

```
@function BZ2_bzCompressInit ___________________________________________________________________
Some documentation about the function here
Prepares for compression. The bz_stream structure holds all data pertaining to the compression 
activity. A bz_stream structure should be allocated and initialised prior to the call. The fields 
of bz_stream comprise the entirety of the user-visible data. state is a pointer to the private 
data structures required for compression.

Custom memory allocators are supported, via fields bzalloc, bzfree, and opaque. The value opaque 
is passed to as the first argument to all calls to bzalloc and bzfree, but is otherwise ignored by 
the library. The call bzalloc ( opaque, n, m ) is expected to return a pointer p to n * m bytes of 
memory, and bzfree ( opaque, p ) should free that memory.

If you don't want to use a custom memory allocator, set bzalloc, bzfree and opaque to NULL, and 
the library will then use the standard malloc / free routines.

Before calling BZ2_bzCompressInit, fields bzalloc, bzfree and opaque should be filled appropriately, 
as just described. Upon return, the internal state will have been allocated and initialised, and 
total_in_lo32, total_in_hi32, total_out_lo32 and total_out_hi32 will have been set to zero. These 
four fields are used by the library to inform the caller of the total amount of data passed into 
and out of the library, respectively. You should not try to change them. As of version 1.0, 64-bit 
counts are maintained, even on 32-bit platforms, using the _hi32 fields to store the upper 32 bits 
of the count. So, for example, the total amount of data in is (total_in_hi32 << 32) + total_in_lo32.

@return int ___________________________________________________________________
BZ_CONFIG_ERROR
  if the library has been mis-compiled
BZ_PARAM_ERROR
  if strm is NULL 
  or blockSize < 1 or blockSize > 9
  or verbosity < 0 or verbosity > 4
  or workFactor < 0 or workFactor > 250
BZ_MEM_ERROR 
  if not enough memory is available
BZ_OK 
  otherwise

@parameter strm: bz_stream* ___________________________________________________________________

@parameter blockSize100kL: int ___________________________________________________________________
Parameter blockSize100k specifies the block size to be used for compression. It should be a value between 1 and 9 inclusive, and the actual block size used is 100000 x this figure. 9 gives the best compression but takes most memory.

@parameter verbosity: int ___________________________________________________________________
Parameter verbosity should be set to a number between 0 and 4 inclusive. 0 is silent, and greater numbers give increasingly verbose monitoring/debugging output. If the library has been compiled with -DBZ_NO_STDIO, no such output will appear for any verbosity setting.

@parameter workFactor: int ___________________________________________________________________
Parameter workFactor controls how the compression phase behaves when presented with worst case, highly repetitive, input data. If compression runs into difficulties caused by repetitive data, the library switches from the standard sorting algorithm to a fallback algorithm. The fallback is slower than the standard algorithm by perhaps a factor of three, but always behaves reasonably, no matter how bad the input.

Lower values of workFactor reduce the amount of effort the standard algorithm will expend before resorting to the fallback. You should set this parameter carefully; too low, and many inputs will be handled by the fallback algorithm and so compress rather slowly, too high, and your average-to-worst case compression times can become very large. The default value of 30 gives reasonable behaviour over a wide range of circumstances.

Allowable values range from 0 to 250 inclusive. 0 is a special case, equivalent to using the default value of 30.

Note that the compressed output generated is the same regardless of whether or not the fallback algorithm is used.

Be aware also that this parameter may disappear entirely in future versions of the library. In principle it should be possible to devise a good way to automatically choose which algorithm to use. Such a mechanism would render the parameter obsolete.

@version 1.0.8```

Each piece of the CAPI file starts with a header in form of

    @<module> <args> <two or more '_'s>

`module` can be `function`, `type`, `object` or `group`

which is followed by a possibly very detailed description, followed by zero or more elements
in form of 

    @<element> <args> <two or more '_'s>

where `element` can be `parameter`, `return`, `version`, etc

__Descriptions are all in MARKDOWN format.__


Elements
========

group <name>
------------

Often several functions belong to the same group. For an example libzstd has many functions that
belong to the "advanced" API. In this case we will have groups/advanced.capi which contains 
`@group advanced` header with description for the entire group. This will go into the generated
code, and those functions that belong to the same group will be in the generated code together,
possibly even in separate module.

Name `core` is reserved as that is the name of the default group.

ignore.list
===========

In each section there can be an `ignore.list` file containing list of ignored symbols.
By default, the first time everything gets generated ALL symbols will be in the ignore.list.
Gradually, deveoper(s) will remove symbol from ignore.list, and create `<symbol>.capi` file in the 
adequate directory to write a detailed info about that particular symbol.

